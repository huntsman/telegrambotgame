<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 12.07.2018
 * Time: 20:39
 */

namespace console\controllers;


use React;
use console\game\echobot;
use yii\console\Controller;

class TestController extends Controller
{
    /**
     *
     */
    public function actionRun()
    {
        $loop = React\EventLoop\Factory::create();
        $GLOBALS["loop"] = $loop;
        $bot = new echobot();
        $update_shift = $bot->currentShift();
        $isActive = false;

        $timer = $loop->addPeriodicTimer(6, function () use ($bot, &$isActive,&$update_shift) {
            if(!$isActive) {
                echo 'Start Script!' . PHP_EOL;
                $isActive = true;
                $bot->update($update_shift);
                $isActive = false;
                echo 'Stop Script!' . PHP_EOL;
            }
        });

        /*$loop->addTimer(1.0, function () use ($loop, $timer) {
            $loop->cancelTimer($timer);
            echo 'Done' . PHP_EOL;
        });*/

        $loop->run();
    }
}