<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 24.07.2018
 * Time: 20:58
 */

namespace console\game\scripts\all;


use console\game\models\tableExploring;
use console\game\models\item;
use console\game\models\itemInPlace;
use console\game\models\pItems;
use console\game\objects\chat;
use console\game\scripts\iscript;

class takeItem implements iscript
{

    public function runBefore(chat $chat)
    {
        $str = strstr($chat->income->text, '_');
        $itemId = ltrim($str, "_");
        $bufferKey = $chat->player->Buffer->searchItemKey(['id' => $itemId, 'name' => 'loot']);

        if ($bufferKey !== false) {
            $item = item::find()->where(['id' => $itemId])->one();
            $pItem = pItems::find()->where([
                'and',
                ['player_id' => $chat->player->id],
                ['item_id' => $itemId]
            ])->one();

            if (!empty($pItem)) {
                ++$pItem->count;
            } else {
                $pItem = new pItems();
                $pItem->player_id = $chat->player->id;
                $pItem->item_id = $itemId;
                $pItem->count = 1;
            }

            if ($pItem->save(['runValidation' => 'false'])) {
                $loot = $chat->player->Buffer->getItemByKey($bufferKey);
                if($loot->flag > 1){
                    --$loot->flag;
                }else{
                    $chat->player->Buffer->removeByKey($bufferKey);
                }
                $msg = $item->name . " теперь у вас в инвентаре";
                $chat->output->setText($msg);
            } else {
                $chat->sendMsg("Ошибка, не удаётся сохранить предмет!");
            }
        } else {
            $chat->sendMsg("Ошибка, предмет не найден!");
        }
    }

    public function runAfter(chat $chat)
    {
        // TODO: Implement runAfter() method.
    }
}