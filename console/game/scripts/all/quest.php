<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 08.09.2018
 * Time: 17:13
 */

namespace console\game\scripts\all;


use console\game\models\quests;
use console\game\objects\chat;
use console\game\scripts\iscript;

class quest implements iscript
{

    public function runBefore(chat $chat)
    {
        $str = strstr($chat->income->text, '_');
        $questId = ltrim($str, "_");
        $bufferKey = $chat->player->Buffer->searchItemKey(['id' => $questId, 'name' => 'quest']);

        if(!empty($bufferKey)){
            $quest = quests::find()->where(['id' => $questId])->one();
            $quest->selectQuest($chat->player);

            $msg = '*' . $quest->name . '*' . PHP_EOL . PHP_EOL;
            $msg .= $quest->discr . PHP_EOL . PHP_EOL;

            if($quest->type == 'kill'){
                $msg .= 'Уничтожьте следующие цели:' . PHP_EOL;
                $allNpc = $quest->getReqNpcOrGo();
                if(is_array($allNpc)){
                    foreach ($allNpc as $npc){
                        $msg .= '*' . $npc['item']->name . '* (x*' . $npc['count'] . '*)' . PHP_EOL;
                    }
                }
            }elseif ($quest->type == 'item'){
                $msg .= 'Соберите следующие предметы:' . PHP_EOL;
                $allItem = $quest->getReqItems();
                if(is_array($allItem)){
                    foreach ($allItem as $item){
                        $msg .= '*' . $item['item']->name . '* (x*' . $item['count'] . '*)' . PHP_EOL;
                    }
                }
            }
            $chat->output->addButton('принять');
            $chat->output->addButton('отказать');
        }
        $chat->output->setText($msg);
    }

    public function runAfter(chat $chat)
    {
        // TODO: Implement runAfter() method.
    }
}