<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 19.07.2018
 * Time: 19:20
 */

namespace console\game\scripts\all;

use console\game\objects\chat;
use console\game\scripts\iscript;


class endDialog implements iscript
{
    public function runBefore(chat $chat)
    {
        // TODO: Implement runBefore() method.
    }

    public function runAfter(chat $chat)
    {
        $chat->sendMsg('Через 2 минуты вы получите то самое сообщение.');
        $chat->sendDelayedMsg('То самое сообщение!', 120000);
    }
}