<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 05.09.2018
 * Time: 15:57
 */

namespace console\game\scripts\all;


use console\game\models\tableExploring;
use console\game\models\item;
use console\game\models\npc_loot;
use console\game\models\tableNpc;
use console\game\objects\chat;
use console\game\service\questService;
use console\game\scripts\iscript;

class attackNpc implements iscript
{

    private $lootId = null;
    private $rewEnergy = null;
    private $complQuest = null;

    public function runBefore(chat $chat)
    {
        $npcBuffer = $chat->player->Buffer;
        $bufferKey = $chat->player->Buffer->getKey('sel', 'flag');
        $npcId = $chat->player->Buffer->getItemByKey($bufferKey)->id;

        $playerDamage = 3;

        $newMsg = 'Ошибка!';
        if($bufferKey !== false){
            $npc = tableNpc::find()->where(['id' => $npcId])->one();
            $enemyHealth = rand($npc->healthMin, $npc->healthMax);
            $enemyDmg = rand($npc->dmgMin, $npc->dmgMax);

            $attacker = rand(0,1);
            $newMsg = 'Битва с ' . $npc->name . PHP_EOL . PHP_EOL;
            while ($chat->player->health > 0 && $enemyHealth > 0){
                if($attacker){
                    $newMsg .= $npc->name . ' нанёс вам ' . $enemyDmg . ' урона' . PHP_EOL;
                    $chat->player->health -= $enemyDmg;
                }else{
                    $newMsg .= 'Вы нанёсли ' . $npc->name . ' ' . $playerDamage . ' урона' . PHP_EOL;
                    $enemyHealth -= $playerDamage;
                }
                $attacker = !$attacker;

            }
            $newMsg .= PHP_EOL;
            if($chat->player->health <= 0){
                $newMsg .= 'Вы повержены!!';
            }elseif ($enemyHealth <= 0) {
                $newMsg .= 'Противник повержен!';

                $service = new questService($chat->player);

                $answ = $service->update($npc->id, 'npc');
                if(!empty($answ)){
                    $complQuest = $answ;
                }

                $chat->player->Buffer->removeByKey($bufferKey);
                $npcExpl = new tableExploring();
                $npcExpl->player_id = $chat->player->id;
                $npcExpl->var_id = $npc->id;
                $npcExpl->var_name = 'npc';
                $npcExpl->date = time() + $npc->respawnTime;
                $npcExpl->save(['runValidation' => 'false']);

                $expl = tableExploring::find()->where([])->all();




                $this->lootId = $npc->loot_id;
                $this->rewEnergy = rand($npc->energy_rew_min, $npc->energy_rew_max);


            }


        }


        $chat->output->setText($newMsg);

    }

    public function runAfter(chat $chat)
    {
        $msg = '';
        if(!empty($this->lootId)) {
            $chat->player->Buffer->removeBy('loot', 'name');
            $msg = 'Найдены предметы:' . PHP_EOL . PHP_EOL;
            $allLoot = npc_loot::find()->where(['id' => $this->lootId])->all();
            if (is_array($allLoot)) {
                $itemQuery = item::find();
                foreach ($allLoot as $loot) {
                    $chance = rand(0, 100);
                    if ($chance < $loot->dropChance) {
                        $itemQuery->orWhere(['id' => $loot->item]);
                    }
                }
                if (!empty($itemQuery->where)) {
                    $items = $itemQuery->all();
                }
                if (is_array($items)) {
                    foreach ($items as $item) {
                        $msg .= $item->name . ' (/takeItem_' . $item->id . ')' . PHP_EOL;
                        foreach ($allLoot as $loot){
                            if($item->id == $loot->item){
                                $chat->player->Buffer->addItem($item->id, 'loot', rand($loot->minCount, $loot->maxCount));
                                break;
                            }

                        }
                    }
                }
            }
        }
        if(!empty($this->rewEnergy)){
            $chat->player->saved_energy += $this->rewEnergy;
            $msg .= PHP_EOL . 'получено' . $this->rewEnergy . 'энергии';
        }
        $chat->sendMsg($msg);

        if(!empty($complQuest)){
            $msg .= 'Выполен квест ' . $complQuest->name;
        }
        $chat->sendMsg($msg);

    }
}