<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 04.09.2018
 * Time: 23:31
 */

namespace console\game\scripts\all;


use console\game\models\tableExploring;
use console\game\models\npc_quests;
use console\game\models\quests;
use console\game\objects\chat;
use console\game\objects\player;
use console\game\scripts\iscript;
use yii\helpers\ArrayHelper;

class npc implements iscript
{


    public function runBefore(chat $chat)
    {
        $str = strstr($chat->income->text, '_');
        $npcId = ltrim($str, "_");
        $bufferKey = $chat->player->Buffer->searchItemKey(['name' => 'npc', 'id' => $npcId]);
        //$chat->player->Buffer->removeFlag('npc', 'sel');

        $newMsg = 'Ошибка!';
        if($bufferKey !== false) {
            $npc = \console\game\objects\npc::find()->where(['id' => $npcId])->one();
            $npc->selectNpc($chat->player);
            $chat->output->addButton('атаковать');
            $newMsg = $npc->getFullDescr($chat->player);

            $chat->player->Buffer->data[$bufferKey]->flag = 'sel';
        }

        $chat->output->addButton('назад');
        $chat->output->setText($newMsg);
        // TODO: Implement runBefore() method.
    }

    public function runAfter(chat $chat)
    {
        // TODO: Implement runAfter() method.
    }
}