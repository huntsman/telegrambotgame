<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 24.07.2018
 * Time: 16:29
 */

namespace console\game\scripts\all;


use console\game\models\item;
use console\game\models\pItems;
use console\game\objects\chat;
use console\game\scripts\iscript;


class items implements iscript
{

    public function runBefore(chat $chat)
    {
        $str = strstr($chat->income->text, '_');
        $itemId = ltrim($str, "_");

        $item = item::find()->where(['id' => $itemId])->one();
        $pItemIds = pItems::find()->where(['and', ['player_id' => $chat->player->id], ['item_id' => $itemId]])->all();
        if (!empty($pItemIds)) {
            $text = $item->name . PHP_EOL;
            $text .= $item->discr. PHP_EOL;
            $chat->output->setText($text);
        }else{
            $chat->sendMsg("У вас нет такого предмета");
        }


        //$playerItems

        // TODO: Implement runBefore() method.
    }

    public function runAfter(chat $chat)
    {
        // TODO: Implement runAfter() method.
    }
}