<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 11.09.2018
 * Time: 15:44
 */

namespace console\game\scripts\all;


use console\game\models\answer;
use console\game\models\tableExploring;
use console\game\models\gameobjects;
use console\game\models\npc_go;
use console\game\objects\area;
use console\game\objects\chat;
use console\game\scripts\iscript;
use yii\helpers\ArrayHelper;

class newPlace implements iscript
{

    public function runBefore(chat $chat)
    {
        $area = area::find()->where(['id' => $chat->player->area])->one();
        $timeNow = time();
        $command = tableExploring::deleteAll(['and', ['!=', 'date', 0], ['<', 'date', $timeNow]]);
        $chat->player->Buffer->clear();
        //$sql = $command->getRawSql();

        print_r($timeNow);
        print_r(PHP_EOL);

        //$situation = 0;
        $situation = 3;//rand(0, 3);

        switch ($situation){
            case 0:
                $chat->output->setText('Похоже здесь ничего нет.');
                break;
            case 1:
                $this->newArea($chat, $area);
                break;
            case 2:
                $this->rndMeeting($chat, $area);
                break;
            case 3:
                $this->rndPlace($chat, $area);
                break;
        }



        $data[] = '';


        print_r('msg: ');
        print_r($chat->output->text);
        print_r(PHP_EOL);



        /*print_r($go_query->createCommand()->rawSql);
        print_r(PHP_EOL);
        print_r($npc_query->createCommand()->rawSql);
        print_r(PHP_EOL);

        print_r($go_array);
        print_r(PHP_EOL);
        print_r($npc_array);
        print_r(PHP_EOL);*/



        /*$expl_arr = ArrayHelper::toArray($expl);
        $cont_arr = ArrayHelper::toArray($place->Container);

        $ttemp = array_diff($cont_arr, $expl_arr);



        $go_expl = array_filter($expl_arr, function($k) {
            return $k['var_type'] == 'object';
        });
        $npc_expl = array_filter($expl_arr, function($k) {
            return $k['var_type'] == 'npc';
        });


        $go_cont = array_filter($cont_arr, function($k) {
            return $k['var_name'] == 'go';
        });
        $npc_cont = array_filter($cont_arr, function($k) {
            return $k['var_name'] == 'npc';
        });*/


        //die();


        /*$newMsg = $tableArea->name . PHP_EOL;
        $newMsg .= $place->discr . PHP_EOL;
        if (!empty($freeItems)) {
            $newMsg .= "Вещи:" . PHP_EOL;
            foreach ($freeItems as $fitem) {
                $newMsg .= $fitem['item']->name;
                $iAmount = $fitem['amount'];
                if ($iAmount > 1) {
                    $newMsg .= "  x" . $iAmount;
                }
                $newMsg .= ' ' . "/takeItem_" . $fitem['item']->id . PHP_EOL;
            }
        }*/

        //$chat->output->setText($allAnswers);

        //$place->items = implode(':', [1,2,3,4,5,6]);
        //$place->save(['runValidation' => 'false']);

        //$newPlace = $this->newPlace($place->id, $tableArea);
        if (!empty($newPlace)) {
            $chat->player->place = $newPlace;
            $chat->player->place_old = $place->id;
        }
    }

    private function newArea(chat $chat, area $area)
    {
        $expl = ArrayHelper::toArray(tableExploring::find()->where(['and', ['player_id' => $chat->player->id], ['var_name' => 'area']])->all());
        $explIds = array_column($expl, 'var_id');
        $explIds[] = $area->id;

        $allArea = area::find()->where(['and', ['>', 'x' , $area->x + $chat->player->squareOfView], ['<', 'x' , $area->x - $chat->player->squareOfView]])
            ->andWhere(['and', ['>', 'y' , $area->y + $chat->player->squareOfView], ['<', 'y' , $area->y - $chat->player->squareOfView]])
            ->andWhere(['not in', 'id', $explIds])
            ->all();

        $findedArea = '';
        foreach ($allArea as $area){
            $rnd = rand(1, 100);
            if($area->chanceToFind >= $rnd){
                $findedArea = $area;
            }

            if(!empty($findedArea)){
                $chat->output->text = 'Вы нашли новую область - ' . $findedArea->name;
                $chat->output->addButton('перейти');
                $chat->output->addButton('остаться');

            }

        }
    }

    /**
     * @param chat $chat
     * @param area $area
     * @return void
     */
    private function rndMeeting(chat $chat, area $area)
    {
        $allNpc = npc_go::find()->where(['and', ['area_id' => $area->id], ['type' => 'npc']])->all();
        $totalNpc = count($allNpc);
        $npcId = $this->selectRndNpcId($allNpc, $totalNpc);

        if(is_null($npcId)){
            $chat->output->text = 'Никого не видно';
            $chat->output->addButton('искать дальше');
            return;
        }

        $npc = \console\game\objects\npc::find()->where(['id' => $npcId])->one();

        if(!empty($npc)){
            $npc->selectNpc($chat->player);
            $chat->output->text = $npc->getFullDescr($chat->player);
            if($npc->danger == 1){
            $chat->output->addButton('атаковать');
            $chat->output->addButton('сбежать');
            }else{
                //$chat->output->addButton('торговать');
                //$chat->output->addButton('говорить');
                $chat->output->addButton('искать дальше');
            }
        }
    }



    private function selectRndNpcId($allNpc, $totalNpc)
    {
        static $limeter = 0;
        $selectedRow = rand(0, $totalNpc - 1);
        $chance = rand(1, 100);
        if ($allNpc[$selectedRow]->chance >= $chance) {
            return $allNpc[$selectedRow]->npcOrGo;
        }

        if ($limeter > 9) {
            return null;
        }

        ++$limeter;
        return $this->selectRndNpcId($allNpc, $totalNpc);
    }

    private function rnd4Object($object)
    {
        $total = count($object);
        $selected = [];

        $array = $this->rnd4($total - 1);

        print_r('object: ');
        print_r($object);
        print_r(PHP_EOL);

        print_r('array: ');
        print_r($array);
        print_r(PHP_EOL);

        foreach ($array as $a) {
            $selected[] = $object[$a];
        }
        return $selected;
    }

    private function rnd4($total, $array = [])
    {
        $rnd = rand(0, $total);
        $totalRow = count($array);
        if ($totalRow > 3) {
            return $array;
        }

        if (!in_array($rnd, $array)) {
            $array[] = $rnd;
        }

        return $this->rnd4($total, $array);
    }

    private function rndPlace(chat $chat, area $area)
    {
        $expl = tableExploring::find()->where(['player_id' => $chat->player->id])->all();
        $allObject = $area->getNpcAndGo($expl);


        if (!empty($allObject)) {
            $npc_array = ArrayHelper::toArray($allObject['npc']);
            $go_array = ArrayHelper::toArray($allObject['go']);

            /*if (is_array($npc_array)) {
                foreach ($npc_array as $npc) {
                    if ($npc['danger'] == 1) {
                        $npc_enemy[] = ['id' => $npc['id'], 'name' => $npc['name'], 'descr' => $npc['descr']];
                    } elseif ($npc['danger'] == 2) {
                        $npc_neutral[] = ['id' => $npc['id'], 'name' => $npc['name'], 'descr' => $npc['descr']];
                    } elseif ($npc['danger'] == 3) {
                        $npc_friend[] = ['id' => $npc['id'], 'name' => $npc['name'], 'descr' => $npc['descr']];
                    }
                }
            }*/

            $danger = array(1 => '\\[враг] ', 2 => '\\[нейтрал] ', 3 => '\\[друг] ');

            $msg = '';
            if (is_array($npc_array)) {
                if (count($npc_array) > 4) {
                    $npc_array = $this->rnd4Object($npc_array);
                }
                $msg .= 'Вы нашли слудующих существ:' . PHP_EOL;
                foreach ($npc_array as $npc){
                    $msg .= $danger[$npc['danger']] . '*' . $npc['name'] . '*' . ' (/npc\\_' . $npc['id'] . ')' . PHP_EOL;
                    //$msg .= $danger[$npc['danger']] . PHP_EOL;
                }

            }
            if (is_array($go_array)) {
                if (count($go_array) > 4) {
                    $go_array = $this->rnd4Object($go_array);
                }
                $msg .= 'Вы нашли слудующие объекты:' . PHP_EOL;
                foreach ($go_array as $go) {
                    if ($go['canBeUsed']) {
                        $link = ' (/go\\_' . $go['id'] . ')';
                    }else{
                        $link = '';
                    }
                    $msg .= '*' . $go['name'] . '*' . $link . PHP_EOL;
                }
            }


            /*$allAnswers = $this->choiceAnswer($npc_enemy, 'npc_enemy');
            $allAnswers .= $this->choiceAnswer($npc_neutral, 'npc_neutral');
            $allAnswers .= $this->choiceAnswer($npc_friend, 'npc_friend');
            $allAnswers .= $this->choiceAnswer($go_array, 'obj_find');*/

            $chat->output->setText($msg);
            $chat->output->addButton('искать дальше');
            //$chat->output->addButton('сбежать');


            foreach ($npc_array as $npc) {
                $chat->player->Buffer->addItem($npc['id'], 'npc', '');
            }
            foreach ($go_array as $go) {
                if ($go['canBeUsed']) {
                    $chat->player->Buffer->addItem($go['id'], 'go', '');
                }
            }
        }

    }


    private function choiceAnswer($array, $type)
    {
        if (is_array($array)) {
            $arrayQuantity = count($array);
            switch ($type) {
                case 'npc_enemy':
                    break;
                case 'npc_friend':
                    break;
                case 'npc_neutral':
                    break;
                case 'go':
                    break;
                case 'obj_none':
                    break;
            }
            $answerQuery = answer::find()->where(['type' => $type]);
            $answersByType = ArrayHelper::toArray($answerQuery->all());
            $answers = array_values(array_filter($answersByType, function ($k) use ($arrayQuantity) {
                return $k['quantity'] == $arrayQuantity;
            }));
            $answerQuantity = count($answers);
            if ($answerQuantity > 0) {
                $temp = $answers[rand(0, $answerQuantity - 1)];
                print_r('rand: ');
                print_r(rand(0, $answerQuantity));
                print_r(PHP_EOL);
                $answer = $temp['text'];
            }

            $counter = 1;
            foreach ($array as $model) {

                $model_name = $model['name'];
                $model_discr = $model['descr'];
                $model_link = '';
                if (strripos($type, 'npc') !== false) {
                    $model_link = '(/npc\\_' . $model['id'] . ')';
                } elseif (strripos($type, 'obj') !== false) {
                    if ($model['canBeUsed']) {
                        $model_link = '(/obj\\_' . $model['id'] . ')';
                    }
                }

                if (!empty($model_name)) {
                    $answer = str_replace('{name' . $counter . '}', $model_name, $answer);
                }
                if (!empty($model_discr)) {
                    $answer = str_replace('{discr' . $counter . '}', $model_discr, $answer);
                }
                //if (!empty($model_link)) {
                $answer = str_replace('{link' . $counter . '}', $model_link, $answer);
                //}
                ++$counter;
            }
        }

        if(strlen($answer) > 0){
            $answer .= PHP_EOL;
        }

        return $answer;

    }

    /*private function newPlace($place, tableArea $tableArea)
    {
        $rnd = rand(0, 7);
        $newplace = 0;
        switch ($rnd) {
            case 0:
                $newplace = $place - $tableArea->height - 1;
                break;
            case 1:
                $newplace = $place - $tableArea->height;
                break;
            case 2:
                $newplace = $place - $tableArea->height + 1;
                break;
            case 3:
                $newplace = $place - 1;
                break;
            case 4:
                $newplace = $place + 1;
                break;
            case 5:
                $newplace = $place + $tableArea->height - 1;
                break;
            case 6:
                $newplace = $place + $tableArea->height;
                break;
            case 7:
                $newplace = $place + $tableArea->height + 1;
                break;
        }

        if (($tableArea->min_id < $newplace) && ($tableArea->max_id > $newplace) && ($place != $newplace)) {
            return $newplace;
        } else {
            $newplace = $this->newPlace($place, $tableArea);
            return $newplace;
        }
    }*/

    public function runAfter(chat $chat)
    {
        // TODO: Implement runAfter() method.
    }

}