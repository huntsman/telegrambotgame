<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 08.09.2018
 * Time: 16:58
 */

namespace console\game\scripts\all;


use console\game\models\tableExploring;
use console\game\objects\chat;
use console\game\scripts\iscript;

class takeQuest implements iscript
{

    public function runBefore(chat $chat)
    {
        $bufferKey = $chat->player->Buffer->searchItemKey(['name' => 'quest', 'flag' => 'sel']);
        $questId = $chat->player->Buffer->getItemByKey($bufferKey)->id;

        if (!empty($questId)) {
            $expl = new tableExploring();
            $expl->player_id = $chat->player->id;
            $expl->var_id = $questId;
            $expl->var_name = 'questStarted';
            $expl->save(['runValidation' => 'false']);
        }

    }

    public function runAfter(chat $chat)
    {
        // TODO: Implement runAfter() method.
    }
}