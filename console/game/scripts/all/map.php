<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 22.07.2018
 * Time: 18:25
 */

namespace console\game\scripts\all;


use console\game\models\answer;
use console\game\models\tableArea;
use console\game\models\tableExploring;
use console\game\models\gameobjects;
use console\game\models\item;
use console\game\models\itemInPlace;
use console\game\models\tableNpc;
use console\game\objects\area;
use console\game\objects\place;
use console\game\objects\chat;
use console\game\objects\playerBuffer;
use console\game\scripts\iscript;
use yii\helpers\ArrayHelper;

class map implements iscript
{

    public function runBefore(chat $chat)
    {
        $area = area::find()->where(['id' => $chat->player->area])->one();
        $msg = '*' . $area->name . '*' . PHP_EOL;
        $msg .= $area->discr;

        $chat->output->setText($msg);
    }



    public function runAfter(chat $chat)
    {
        // TODO: Implement runAfter() method.
    }
}