<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 20.07.2018
 * Time: 14:55
 */

namespace console\game\scripts\all;


use console\game\objects\chat;
use console\game\scripts\iscript;



class startGame implements iscript
{
    public function runBefore(chat $chat)
    {
        $chat->output->addButton('начать');
    }

    public function runAfter(chat $chat)
    {
        // TODO: Implement runAfter() method.
    }
}