<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 19.07.2018
 * Time: 19:05
 */

namespace console\game\scripts;

use console\game\echobot;
use console\game\objects\chat;

interface iscript
{
    public function runBefore(chat $chat);
    public function runAfter(chat $chat);
}