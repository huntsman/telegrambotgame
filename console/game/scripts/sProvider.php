<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 19.07.2018
 * Time: 19:13
 */

namespace console\game\scripts;


use console\game\objects\chat;
use ReflectionClass;
use console\game\scripts\iscript;

/**
 * sProvider
 *
 * @property iscript $script
 *
 */
class sProvider
{
    public $script;

    public function init($scriptName)
    {
        if (!empty($scriptName)) {
            try {
                $commandClass = new ReflectionClass('console\game\scripts\all\\' . $scriptName);
            } catch (\ReflectionException $e) {
                //логирование
            }

            if (!is_null($commandClass)) {
                if ($commandClass->isSubClassOf('console\game\scripts\iscript')) {

                    $this->script = $commandClass->newInstance();
                }
            }
        }
    }

    public function beforeScript(chat $chat)
    {
        if(!is_null($this->script)) {
            $this->script->runBefore($chat);
        }
    }
    public function afterScript(chat $chat)
    {
        if(!is_null($this->script)) {
            $this->script->runAfter($chat);
        }
    }

}