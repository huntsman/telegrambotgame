<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 02.09.2018
 * Time: 0:26
 */

namespace console\game\objects;

/**
 * playerBuffer object
 *
 * @property bElement $data
 *
 */
class playerBuffer
{
    /*public $var_id;
    public $var_name;
    public $var_value;*/

    public $data;



    public function clear(){
        $this->data = [];
    }

    public function getItemByKey($key){
        return $this->data[$key];
    }

    public function addItem($id, $name, $flag)
    {
        //$this->data[] = ['id' => $id, 'name' => $name, 'flag' => $flag];
        $this->data[] = new bElement($id, $name, $flag);
    }

    /**
     * @param $needle
     * @param $field
     * @return bool|int|string
     */
    public function getKey($needle, $field)
    {
        $findedKey = false;
        foreach ($this->data as $key => $data) {
            if ($data->{$field} == $needle) {
                $findedKey = $key;
                break;
            }
        }
        return $findedKey;
    }

    /**
     * @param $param
     * @return bool|int|string
     */
    public function searchItemKey($param)
    {
        $findedKey = false;
        if (is_array($this->data)) {
            foreach ($this->data as $key => $data) {
                if ($findedKey === false) {
                    if (is_array($param)) {
                        $exits = true;
                        foreach ($param as $field => $needle) {
                            if ($exits) {
                                if ($data->{$field} != $needle) {
                                    $exits = false;

                                }
                            } else {
                                break;
                            }
                        }
                        if ($exits) {
                            $findedKey = $key;
                        }
                    }
                } else {
                    break;
                }
            }
        }
        return $findedKey;
    }

    public function removeByKey($key)
    {
        unset($this->data[$key]);
    }

    public function removeFlag($name, $flag)
    {
        foreach ($this->data as $key => $element) {
            if ($element->flag == $flag && $element->name == $name) {
                $this->data[$key]->flag = '';
            }
        }
    }

    public function removeBy($needle, $field)
    {
        foreach ($this->data as $key => $element) {
            if ($element->{$field} == $needle) {
                unset($this->data[$key]);
            }
        }
    }

    /*public function getByName($name)
    {
        $var = array_filter($this->data, function ($k) use ($name) {
            return $k['name'] == $name;
        });
        return $var;
    }

    public function getByNameAndFlag($name, $flag)
    {
        $var = array_filter($this->data, function ($k) use ($name, $flag) {
            return ($k['name'] == $name) && ($k['flag'] == $flag);
        });
        return $var;
    }*/



}