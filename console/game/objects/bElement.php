<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 05.09.2018
 * Time: 23:45
 */

namespace console\game\objects;

/**
 * bElement object
 *
 * @property integer $id
 * @property integer $name
 * @property integer $flag
 *
 */
class bElement
{
    public $id;
    public $name;
    public $flag;

    public function __construct($id, $name, $value)
    {
        $this->id = $id;
        $this->name = $name;
        $this->flag = $value;
    }

}