<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 20.07.2018
 * Time: 18:45
 */

namespace console\game\objects;



use Yii;
use console\game\models\inMsg;
use console\game\models\outMsg;

/**
 * chat model
 *
 * @property integer $id
 * @property player $player
 * @property outMsg $income
 * @property outMsg $output
 *
 */
class chat
{
    public $id;
    public $player;
    public $income;
    public $output;

    public function __construct(inMsg $msg)
    {
        $input = $msg;
        $this->id = $input->chat_id;
        $this->player = player::find()->where(['chat' => $input->chat_id])->one();
        if (is_null($this->player)) {
            $this->player = new player();
            $this->player->chat = $msg->chat_id;
            $this->player->mode = 'dialog';
            $this->player->save(['runValidation' => 'false']);
        }

        $this->output = new outMsg($input->chat_id);
    }

    public function sendMessage()
    {
        $json = Yii::$app->telegram->sendMessage($this->output->getMessage());
        if (!empty($json)) {
            return true;
        }
        return false;
    }

    public function sendDelayedMessage($ms)
    {
        $ms *= 0.001;
        $loop = $GLOBALS["loop"];
        $loop->addTimer($ms, function (){
            $this->sendMessage();
        });
    }

    public function sendMsg($text)
    {
        $json = Yii::$app->telegram->sendMessage(['chat_id' => $this->id, 'text' => $text]);
        if (!empty($json)) {
            return true;
        }
        return false;
    }

    public function sendDelayedMsg($text, $ms)
    {
        $ms *= 0.001;
        $loop = $GLOBALS["loop"];
        $loop->addTimer($ms, function () use ($text) {
            Yii::$app->telegram->sendMessage(['chat_id' => $this->id, 'text' => $text]);
        });
    }

}