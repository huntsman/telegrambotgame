<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 12.09.2018
 * Time: 20:31
 */

namespace console\game\objects;


use console\game\models\tableExploring;
use console\game\models\npc_quests;
use console\game\models\quests;
use console\game\models\tableNpc;
use yii\helpers\ArrayHelper;

class npc extends tableNpc
{

    private $_quests;

    public function selectNpc($player)
    {
        $player->Buffer->removeFlag('npc', 'sel');
        $player->Buffer->removeBy('quest', 'name');

        $buffKey = $player->Buffer->searchItemKey(['id' => $this->id, 'name' => 'npc']);
        if (!empty($buffKey)) {
            $player->Buffer->getItemByKey($buffKey)->flag = 'sel';

            $quests = $this->getQuests($player);
            print_r('quests:');
            print_r(PHP_EOL);
            print_r($quests);
            print_r(PHP_EOL);

            if (is_array($quests)) {
                foreach ($quests as $quest) {
                    $player->Buffer->addItem($quest['id'], 'quest', '');
                }
            }
        }
    }

    public function getFullDescr($player)
    {
        $fullDescr = '*' . $this->name . '*' . PHP_EOL;
        $fullDescr .= $this->descr . PHP_EOL;
        $fullDescr .= PHP_EOL . 'Вероятный уровень: ' . $this->lvl_min . ' - ' . $this->lvl_max . PHP_EOL;

        if ($this->danger > 1) {
            $quests = $this->getQuests($player);
            if (is_array($quests)) {
                $fullDescr .= PHP_EOL . 'У меня есть задания для вас:' . PHP_EOL;
                foreach ($quests as $quest) {
                    $fullDescr .= $quest['name'] . ' (/quest\\_' . $quest['id'] . ')' . PHP_EOL;
                }
            }
        }
        return $fullDescr;
    }


    public function getQuests(player $player)
    {
        if (empty($this->_quests)) {
            $npcQuests = npc_quests::find()->where(['npc' => $this->id])->all();

            if (!empty($npcQuests)) {
                $exploring = ArrayHelper::toArray(tableExploring::find()->where([
                    'and',
                    ['player_id' => $player->id],
                    ['var_name' => 'questDone']
                ])->all());
                $expl_ids = array_column($exploring, 'var_id');

                $allQuestQuery = quests::find();
                foreach ($npcQuests as $npcQuest) {
                    $allQuestQuery->orWhere(['id' => $npcQuest->quest]);
                }
                if (!empty($allQuestQuery->where)) {
                    $allQuestQuery->andWhere(['not in', 'id', $expl_ids]);
                    $allQuest = ArrayHelper::toArray($allQuestQuery->all());

                    $playerLvl = $player->lvl;

                    $quests = array_filter($allQuest, function ($k) use ($playerLvl, $expl_ids) {

                        if ($k['minLevel'] <= $playerLvl) {
                            if (empty($k['prevQuest'])) {
                                return true;
                            } else {
                                foreach ($expl_ids as $expl_id) {
                                    if ($expl_id == $k['prevQuest']) {
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    });
                    $this->_quests = $quests;
                }

            }
        }
        return $this->_quests;
    }


}