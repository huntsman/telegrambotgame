<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 14.09.2018
 * Time: 23:29
 */

namespace console\game\objects;


use console\game\models\quests;

class takenQuest
{
    public $id;
    public $reqNpcOrGo;
    public $reqItem;
    public $done;

    public $quest;

    public function __construct(quests $quest)
    {
        $this->id = $quest->id;
        $this->quest = $quest;
        $this->done = false;
    }




    public function done()
    {
        $condition1 = false;
        $condition2 = false;

        if($this->quest->type == 'kill'){
            $req = $this->quest->getReqNpcOrGoId();
            foreach ($req as $r){
                if ($r['id'] == $this->reqNpcOrGo['id']) {
                    if($r['count'] <= $this->reqNpcOrGo[0]['count']){
                        $condition1 = true;
                    }
                } else {
                    if($r['count'] <= $this->reqNpcOrGo[1]['count']){
                        $condition2 = true;
                    }
                }

                /*foreach ($reqNpcOrGo as $npc){

                }*/
            }
        }

        if($condition1 && $condition2){
            return true;
        }

        return false;
    }

}