<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 02.09.2018
 * Time: 2:28
 */

namespace console\game\objects;

/**
 * placeContainer object
 *
 * @property int $var_id
 * @property string $var_name
 * @property int $var_chance
 *
 */
class placeContainer
{
    public $var_id;
    public $var_name;
    public $var_chance;

    public function __construct($id, $name, $chance)
    {
        $this->var_id = $id;
        $this->var_name = $name;
        $this->var_chance = $chance;
    }
}