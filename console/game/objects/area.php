<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 12.09.2018
 * Time: 20:35
 */

namespace console\game\objects;


use console\game\models\tableExploring;
use console\game\models\gameobjects;
use console\game\models\npc_go;
use console\game\models\tableArea;

class area extends tableArea
{
    public function getNpc($expl_ids)
    {
        $npcPlace = npc_go::find()->where(['and', ['area_id' => $this->id], ['type' => 'npc']])->all();

        $npcQuery = npc::find();
        foreach ($npcPlace as $row) {
            $chance = rand(1, 100);
            if ($chance <= $row->chance) {
                $npcQuery->orWhere(['id' => $row->npcOrGo]);
            }
        }
        if (!empty($npcQuery->where)) {
            $npcQuery->andWhere(['not in', 'id', $expl_ids]);
            $allNpc = $npcQuery->all();
            if (is_array($allNpc)) {
                return $allNpc;
            }
        }

        return null;
    }

    public function getGo($expl_ids)
    {
        $goPlace = npc_go::find()->where(['and', ['area_id' => $this->id], ['type' => 'go']])->all();

        $goQuery = gameobjects::find();
        foreach ($goPlace as $row) {
            $chance = rand(1, 100);
            if ($chance <= $row->chance) {
                $goQuery->orWhere(['id' => $row->npcOrGo]);
            }
        }
        if (!empty($goQuery->where)) {
            $goQuery->andWhere(['not in', 'id', $expl_ids]);
            $allGo = $goQuery->all();
            if (is_array($allGo)) {
                return $allGo;
            }
        }
        return null;
    }

    public function getNpcAndGo($expl)
    {
        $npcAndGo = npc_go::find()->where(['area_id' => $this->id])->all();

        $go_query = gameobjects::find();
        $npc_query = npc::find();

        foreach ($npcAndGo as $row) {
            $exits = false;
            foreach ($expl as $expl_p) {
                if ($expl_p->var_id == $row->npcOrGo && $expl_p->var_name == $row->type) {
                    $exits = true;
                }
            }

            if (!$exits) {
                $rnd = rand(0, 100);
                if ($row->chance >= $rnd) {
                    if ($row->type == 'go') {
                        $go_query->orWhere(['id' => $row->npcOrGo]);
                    } elseif ($row->type == 'npc') {
                        $npc_query->orWhere(['id' => $row->npcOrGo]);
                    }
                }
            }
        }

        $allNpc = [];
        $allGo = [];

        if (!empty($npc_query->where)) {
            $allNpc = $npc_query->all();
        }
        if (!empty($go_query->where)) {
            $allGo = $go_query->all();
        }

        $allData = ['npc' => $allNpc, 'go' => $allGo];

        return $allData;
    }

}