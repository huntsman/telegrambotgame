<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 12.07.2018
 * Time: 21:16
 */
namespace console\game;

use console\game\models\tableArea;
use console\game\models\command;
use console\game\models\item;
use console\game\models\outMsg;
use console\game\models\inMsg;
use console\game\models\entitiesMsg;
use console\game\models\place;
use console\game\models\players;
use console\game\objects\chat;
use console\game\scripts\sProvider;
use Yii;

class echobot
{
    public function update(&$shiftn)
    {
        $chats = Yii::$app->telegram->getUpdates(['offset' => $shiftn]);
        $temp = $chats->result;

        if ($chats == null) {
            echo 'Data is not available!' . PHP_EOL;
            return false;
        }

        $chatArray[] = '';
        $shift_id = null;

        if (is_array($temp)) {
            foreach ($temp as $chat) {
                $shift_id = $chat->update_id;
                $chat_id = $chat->message->from->id;
                $date = $chat->message->date;
                $text = $chat->message->text;
                $entities = $chat->message->entities;


                if (!in_array(['chat_id' => $chat_id], $chatArray)) {
                    $chatArray[] = ['chat_id' => $chat_id];
                    $msg = new inMsg();
                    $msg->chat_id = $chat_id;
                    $msg->date = $date;
                    $msg->text = $text;

                    if (is_array($entities)) {
                        foreach ($entities as $entity) {
                            $msgentity = new entitiesMsg();
                            $msgentity->length = $entity->length;
                            $msgentity->offset = $entity->offset;
                            $msgentity->type = $entity->type;

                            $msg->entities[] = $msgentity;
                        }
                    }
                    $this->executeCommand($msg);
                }
            }
        }

        if (!is_null($shift_id)) {
            $temp_schift = $shift_id + 1;
            echo 'shift set to ' . $temp_schift . ' old value is ' . $shiftn . PHP_EOL;
            $shiftn = $temp_schift;

        }

        return true;
    }

    public function currentShift()
    {
        $chats = Yii::$app->telegram->getUpdates();
        $temp = $chats->result;

        if ($chats == null) {
            echo 'Data is not available!' . PHP_EOL;
            return false;
        }
        $shift_id = array_pop($temp)->update_id;
        if (!is_null($shift_id)) {
            ++$shift_id;
            return $shift_id;
        }
        return false;
    }

    /**
     * @param inMsg $msg
     */
    public function executeCommand(inMsg $msg)
    {

        $chat = new chat($msg);
        $chat->income = $msg;

        /*switch ($chat->player->mode) {
            case "dialog":*/
                $this->buildDialog($chat);
                /*break;
            case "map":
                $this->buildMap($chat);
                break;
            case "user":

                break;
            default:
                $this->SendMessage($msg->chat_id, 'Ошибка, режим не известен!');
                break;


        }*/

        $chat->player->save(['runValidation' => 'false']);


        /*$command = commands::find()->where(['name' => $msg->text])->one();

        switch ($command->type) {
            case 0:
                $this->SendMessage($msg->chat_id, $command->answer);
                break;
            default:
                $this->SendMessage($msg->chat_id, 'Ошибка, тип сообщения не известен!');
                break;
        }*/
    }

    private function buildDialog(chat &$chat)
    {
        $sProvider = new sProvider();
        $clearMsg = strstr($chat->income->text, '_', true);
        if($clearMsg === false){
            $clearMsg = $chat->income->text;
        }


        print_r('$clearMsg: ');
        print_r($clearMsg);
        print_r(PHP_EOL);





        $lastDialog = command::find()->where(['id' => $chat->player->mode_var])->one();
        if (!is_null($lastDialog)) {
            $dialogStep = is_null($lastDialog->next_step) ? $lastDialog->step : $lastDialog->next_step;
        }

        $query = command::find()->where(['name' => $clearMsg]);
        if (!is_null($lastDialog)) {
            $query->andWhere(['dialog_id' => $lastDialog->dialog_id]);
            $query->andWhere(['step' => $dialogStep]);
        }
        $query->orWhere(['and', ['name' => $clearMsg], ['anywhere' => 1]]);
        $dialog = $query->one();

        if (empty($lastDialog)) {
            $this->SendMessage($chat->id, 'Ошибка, предыдущая команда не найдена!');
        }

        if (empty($dialog)) {
            $this->SendMessage($chat->id, 'Ошибка, команда не найдена!');
            return;
        }

        if (!empty($dialog->goto)) {
            if (is_numeric($dialog->goto)) {
                $dialog = command::find()->where(['id' => $dialog->goto])->one();
            } elseif (is_string($dialog->goto)) {
                switch ($dialog->goto){
                    case "map":
                        $this->buildMap($chat);
                        return;
                        break;
                    case "user":
                        break;
                }
            }
        }

        if (!empty($dialog->script)) {
            $sProvider->init($dialog->script);
        }

        $chat->output->setText($dialog->text);
        $chat->output->addButton($dialog->answer_1);
        $chat->output->addButton($dialog->answer_2);
        $chat->output->addButton($dialog->answer_3);
        $chat->output->addButton($dialog->answer_4);

        $sProvider->beforeScript($chat);
        $sended = $chat->sendMessage();
        if ($sended) {
            $chat->player->mode_var = $dialog->id;
        }
        $sProvider->afterScript($chat);
    }

    private function buildMap(chat &$chat)
    {
        $place = place::find()->where(['id' => $chat->player->place])->one();
        $area = tableArea::find()->where(['id' => $place->area_id])->one();
        $itemId = explode(':', $place->items);
        if (is_array($itemId)) {
            $itemQuery = item::find();
            foreach ($itemId as $iId) {
                $itemQuery->orWhere(['id' => $iId]);
            }
            $items = $itemQuery->all();
        }

        $newMsg = $area->name . PHP_EOL;
        $newMsg .= $place->discr . PHP_EOL;
        if (!empty($items)) {
            $newMsg .= "Вещи:" . PHP_EOL;
            foreach ($items as $item){
                $newMsg .=  $item->name . PHP_EOL;
            }
        }



        $chat->output->setText($newMsg);
        $chat->output->addButton("переход");
        $chat->output->addButton("поиск");
        $chat->output->addButton("персонаж");

        //$place->items = implode(':', [1,2,3,4,5,6]);
        //$place->save(['runValidation' => 'false']);

        $newPlace = '';
        switch ($chat->income->text) {
            case "переход":
                $chat->income->text = 'переход';
                $chat->output->clear();
                $this->buildDialog($chat);

                return;
            case "поиск":

                $newPlace = $this->newPlace($place->id, $area);
                break;
            case "персонаж":
                break;
        }

        $sended = $chat->sendMessage();
        if ($sended) {
            /*if ($chat->player->mode != 'map') {
                $chat->player->mode = 'map';
            }*/
            if(!empty($newPlace)){
                $chat->player->place = $newPlace;
            }


        }
    }

    private function newPlace($place, tableArea $area)
    {
        $rnd = rand(0, 7);
        $newplace = 0;
        switch ($rnd) {
            case 0:
                $newplace = $place - $area->height - 1;
                break;
            case 1:
                $newplace = $place - $area->height;
                break;
            case 2:
                $newplace = $place - $area->height + 1;
                break;
            case 3:
                $newplace = $place - 1;
                break;
            case 4:
                $newplace = $place + 1;
                break;
            case 5:
                $newplace = $place + $area->height - 1;
                break;
            case 6:
                $newplace = $place + $area->height;
                break;
            case 7:
                $newplace = $place + $area->height + 1;
                break;
        }


        if(($area->min_id < $newplace) && ($area->max_id > $newplace) && ($place != $newplace)){
            return $newplace;
        }else{
            $newplace = $this->newPlace($place, $area);
            return $newplace;
        }
    }

    /**
     * @param int $chat_id
     * @param string $text
     */
    private function SendMessage($chat_id, $text)
    {
        Yii::$app->telegram->sendMessage([
            'chat_id' => $chat_id,
            'text' => $text,
        ]);
    }

}