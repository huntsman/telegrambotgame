<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 14.09.2018
 * Time: 22:50
 */

namespace console\game\service;


use console\game\models\quests;
use console\game\objects\exploring;
use console\game\objects\player;

class questService
{
    private $player;
    private $quests;

    public function __construct(player $player)
    {
        $this->player = $player;
        $expl = exploring::find()->where(['and', ['player_id' => $player->id], ['var_name' => 'questStarted']])->all();

        if(is_array($expl)){
            $questQuery = quests::find();
            foreach ($expl as $ex){
                $questQuery->orWhere(['id' => $ex->var_id]);
            }
            if(!empty($questQuery->where)){
                $quests = $questQuery->all();
                $this->quests = $quests;
                /*foreach ($quests as $quest){
                    $quests[] = $quest;

                }*/
            }
        }

    }

    private function updateDone(exploring $expl, quests $quest){
        $condition1 = false;
        $condition2 = false;

        //$expl = exploring::find()->where(['and', ['player_id' => $this->player->id], ['var_name' => 'questNpcOrGo']])->all();



        if($quest->type == 'kill'){
            $req = $quest->getReqNpcOrGoId();
            $counter = 0;
            foreach ($req as $r){
                foreach ($expl as $ex){
                    if ($r['id'] == $ex->var_id) {
                        if($r['count'] <= $ex->var_amount){
                            if($counter){
                                $condition2 = true;
                                break;
                            }else{
                                $condition1 = true;
                            }
                        }
                    }

                }
                ++$counter;
            }
        }

        if($condition1 && $condition2){
            return true;
        }

        return false;

    }

    public function update($objId, $type)
    {
        foreach ($this->quests as $quest) {
            if ($quest->check($objId, $type)) {
                if ($type == 'npc' || $type == 'go') {
                    $explNpcOrGo = exploring::find()->where([
                        'and',
                        ['player_id' => $this->player->id],
                        ['var_name' => 'questNpcOrGo']
                    ])
                        ->andWhere(['var_id' => $objId])
                        ->one();

                    if (!empty($explNpcOrGo)) {
                        ++$explNpcOrGo->var_amount;
                    } else {
                        $explNpcOrGo = new exploring();
                        $explNpcOrGo->player_id = $this->player->id;
                        $explNpcOrGo->var_id = $objId;
                        $explNpcOrGo->var_name = 'questNpcOrGo';
                        $explNpcOrGo->save();
                    }
                }


                $answer = $this->updateDone($explNpcOrGo, $quest);
                $compl = $quest;
            }
        }
        if($answer){
            return $compl;

        }

        return null;

    }

}



