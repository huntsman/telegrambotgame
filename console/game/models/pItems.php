<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 24.07.2018
 * Time: 16:58
 */

namespace console\game\models;

use yii\db\ActiveRecord;

/**
 * playerItems model
 *
 * @property integer $id
 * @property integer $player_id
 * @property integer $item_id
 * @property boolean $equipped
 * @property integer $count
 *
 */
class pItems extends ActiveRecord
{
    public static function tableName()
    {
        return 'player_items';
    }

}