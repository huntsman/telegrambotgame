<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 19.07.2018
 * Time: 1:22
 */

namespace console\game\models;


use yii\db\ActiveRecord;

/**
 * players model
 *
 * @property integer $id
 * @property integer $chat
 * @property integer $place
 * @property integer $area
 * @property integer $place_old
 * @property integer $mode_var
 * @property string $mode
 * @property string $buffer
 * @property float $squareOfView
 * @property integer $lvl
 * @property integer $health
 *
 */
class players extends ActiveRecord
{
    public static function tableName()
    {
        return 'players';
    }

}