<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 02.09.2018
 * Time: 17:59
 */

namespace console\game\models;


use yii\db\ActiveRecord;

/**
 * answer model
 *
 * @property int $id
 * @property string $type
 * @property int $quantity
 * @property string $text
 *
 */
class answer extends ActiveRecord
{
    public static function tableName()
    {
        return 'answer_template';
    }

}