<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 05.09.2018
 * Time: 17:05
 */

namespace console\game\models;


use yii\db\ActiveRecord;

/**
 * npc_loot model
 *
 * @property integer $id
 * @property integer $item
 * @property integer $dropChance
 * @property integer $minCount
 * @property integer $maxCount
 *
 */
class npc_loot extends ActiveRecord
{

}