<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 22.07.2018
 * Time: 15:19
 */

namespace console\game\models;


use yii\db\ActiveRecord;

/**
 * tableArea model
 *
 * @property integer $id
 * @property string  $name
 * @property string  $discr
 * @property integer $x
 * @property integer $y
 * @property string  $chanceToFind
 *
 */
class tableArea extends ActiveRecord
{
    public static function tableName()
    {
        return 'areas';
    }

}