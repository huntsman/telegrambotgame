<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 19.07.2018
 * Time: 18:12
 */

namespace console\game\models;


use yii\db\ActiveRecord;

/**
 * command model
 *
 * @property integer $id
 * @property integer $dialog_id
 * @property integer $step
 * @property integer $next_step
 * @property integer $anywhere
 * @property string $goto
 * @property string $type
 * @property string $name
 * @property string $text
 * @property string $answer_1
 * @property string $answer_2
 * @property string $answer_3
 * @property string $answer_4
 * @property string $script
 *
 */
class command extends ActiveRecord
{
    public static function tableName()
    {
        return 'commands';
    }

}