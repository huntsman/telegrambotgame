<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 12.07.2018
 * Time: 21:19
 */

namespace console\game\models;


use yii\db\ActiveRecord;
/**
 * chat model
 *
 * @property integer $id
 * @property integer $chat_id
 * @property integer $updated_id
 */
class chat extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%chats}}';
    }

    public function rules()
    {
        return [
            [['chat_id', 'updated_id'], 'integer'],
        ];
    }

}