<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 11.09.2018
 * Time: 15:31
 */

namespace console\game\models;


use yii\db\ActiveRecord;

/**
 * npc_go model
 *
 * @property integer $area_id
 * @property string $npcOrGo
 * @property string $type
 * @property integer $chance
 * @property string $meetingText
 * @property integer $phase
 *
 */
class npc_go  extends ActiveRecord
{

}