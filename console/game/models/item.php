<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 23.07.2018
 * Time: 16:55
 */

namespace console\game\models;

use yii\db\ActiveRecord;

/**
 * item model
 *
 * @property integer $id
 * @property string $name
 * @property string $discr
 * @property string $type
 *
 */
class item extends ActiveRecord
{
    public static function tableName()
    {
        return 'items';
    }

}