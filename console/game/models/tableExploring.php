<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 24.07.2018
 * Time: 22:15
 */

namespace console\game\models;


use yii\db\ActiveRecord;
/**
 * tableExploring model
 *
 * @property integer $id
 * @property integer $player_id
 * @property integer $var_id
 * @property integer $var_name
 * @property integer $var_amount
 * @property integer $date
 *
 */
class tableExploring extends ActiveRecord
{
    public static function tableName()
    {
        return 'player_exploring';
    }



}