<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 22.07.2018
 * Time: 15:19
 */

namespace console\game\models;


use yii\db\ActiveRecord;

/**
 * place model
 *
 * @property integer $id
 * @property integer $area_id
 * @property string $phase
 * @property string $extradiscr
 * @property string $container
 *
 */
class places extends ActiveRecord
{
    public static function tableName()
    {
        return 'places';
    }

}