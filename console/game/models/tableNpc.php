<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 02.09.2018
 * Time: 22:40
 */

namespace console\game\models;


use yii\db\ActiveRecord;

/**
 * tableArea model
 *
 * @property integer $id
 * @property string  $danger
 * @property string  $name
 * @property integer $descr
 * @property integer $health
 * @property integer $armor
 * @property string  $dmg
 * @property string  $lvl_min
 * @property string  $lvl_max
 * @property string  $energy
 * @property string  $exp_rew
 * @property string  $energy_rew_min
 * @property string  $energy_rew_max
 * @property string  $loot_id
 * @property string  $script_name
 * @property string  $respawnTime
 *
 */
class tableNpc extends ActiveRecord
{
    public static function tableName()
    {
        return 'npcs';
    }

}