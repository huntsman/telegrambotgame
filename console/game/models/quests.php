<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 08.09.2018
 * Time: 0:00
 */

namespace console\game\models;


use console\game\objects\player;
use yii\db\ActiveRecord;

/**
 * quests model
 *
 * @property integer $id
 * @property string $name
 * @property string $discr
 * @property integer $minLevel
 * @property string $type
 * @property integer $reqItem1
 * @property integer $reqItemCount1
 * @property integer $reqItem2
 * @property integer $reqItemCount2
 * @property integer $reqNpcOrGo1
 * @property integer $reqNpcOrGoCount1
 * @property integer $reqNpcOrGo2
 * @property integer $reqNpcOrGoCount2
 * @property integer $rewEnergy
 * @property integer $rewItem1
 * @property integer $rewItemCount1
 * @property integer $rewItem2
 * @property integer $rewItemCount2
 * @property string $rewText
 * @property integer $prevQuest
 * @property integer $nextQuest
 *
 */
class quests extends ActiveRecord
{

    public function selectQuest(player $player){
        $player->Buffer->removeFlag('quest', 'sel');
        $buffKey = $player->Buffer->searchItemKey(['id' => $this->id, 'name' => 'quest']);
        if(!empty($buffKey)){
            $player->Buffer->getItemByKey($buffKey)->flag = 'sel';
        }
    }

    public function getReqItems()
    {
        $rItems = [];
        $items = item::find()->where(['or', ['id' => $this->reqItem1], ['id' => $this->reqItem2]])->all();
        foreach ($items as $item) {
            if ($item->id == $this->reqItem1) {
                $rItems[] = ['item' => $item, 'count' => $this->reqItemCount1];
            } else {
                $rItems[] = ['item' => $item, 'count' => $this->reqItemCount2];
            }
        }

        return $rItems;
    }

    public function getReqNpcOrGo($isNpc = true)
    {
        $rNpcOrGo = [];
        $queryPiece = ['or', ['id' => $this->reqNpcOrGo1], ['id' => $this->reqNpcOrGo2]];
        if ($isNpc) {
            $npcOrGo = tableNpc::find()->where($queryPiece)->all();
        } else {
            $npcOrGo = gameobjects::find()->where($queryPiece)->all();
        }
        foreach ($npcOrGo as $item) {
            if ($item->id == $this->reqNpcOrGo1) {
                $rNpcOrGo[] = ['item' => $item, 'count' => $this->reqNpcOrGoCount1];
            } else {
                $rNpcOrGo[] = ['item' => $item, 'count' => $this->reqNpcOrGoCount2];
            }
        }
        return $rNpcOrGo;
    }

    public function getReqNpcOrGoId($isNpc = true)
    {
        $rNpcOrGo[] = ['id' => $this->reqNpcOrGo1, 'count' => $this->reqNpcOrGoCount1];
        $rNpcOrGo[] = ['id' => $this->reqNpcOrGo2, 'count' => $this->reqNpcOrGoCount2];

        return $rNpcOrGo;
    }

    public function getRewItems()
    {
        $rItems = [];
        $items = item::find()->where(['or', ['id' => $this->rewItem1], ['id' => $this->rewItem2]])->all();
        foreach ($items as $item) {
            if ($item->id == $this->rewItem1) {
                $rItems[] = ['item' => $item, 'count' => $this->rewItemCount1];
            } else {
                $rItems[] = ['item' => $item, 'count' => $this->rewItemCount2];
            }
        }

        return $rItems;
    }

    public function check($objId , $type)
    {
        if (!empty($object)) {
            switch ($type) {
                case 'npc':
                    $npcs = $this->quest->getReqNpcOrGo();
                    foreach ($npcs as $npc) {
                        if ($npc['item']->id == $objId) {
                            return true;
                        }
                    }
                    break;
                case 'go':
                    $gos = $this->quest->getReqNpcOrGo(false);
                    foreach ($gos as $go) {
                        if ($go['item']->id == $objId) {
                            return true;
                        }
                    }
                    break;
                case 'item':
                    $items = $this->quest->getRewItems();
                    foreach ($items as $item) {
                        if ($item['item']->id == $objId) {
                            return true;
                        }
                    }
                    break;
            }
        }
        return false;
    }

}