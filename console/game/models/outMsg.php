<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 18.07.2018
 * Time: 23:41
 */

namespace console\game\models;


class outMsg
{
    private $chat;
    public $text;
    private $message;
    private $buttons;

    /**
     * currentmsg constructor.
     * @param int $chat
     */
    public function __construct($chat)
    {
        $this->chat = $chat;
        $this->buttons = [[]];
    }

    /**
     * @param string $text
     */
    public function addButton($text)
    {
        if(!is_null($text)) {
            $this->buttons[0][] = $text;
        }
    }

    public function clear()
    {
        $buttons = $this->buttons[0];
        if (is_array($buttons)) {
            foreach ($buttons as $key => $button) {
                $this->buttons[0][$key] = "";
            }
        }
        $this->text = '';
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        $resp = array("keyboard" => $this->buttons,"resize_keyboard" => true,"one_time_keyboard" => true);
        $reply = json_encode($resp);
        $msg = ['chat_id' => $this->chat, 'text' => $this->text, 'parse_mode' => 'markdown', 'reply_markup' => $reply,];
        return $msg;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }
}