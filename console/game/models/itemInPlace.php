<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 24.07.2018
 * Time: 22:49
 */

namespace console\game\models;


use yii\db\ActiveRecord;
/**
 * itemInPlace model
 *
 * @property integer $id
 * @property integer $place_id
 * @property integer $item_id
 * @property integer $amount
 *
 */
class itemInPlace extends ActiveRecord
{
    public static function tableName()
    {
        return 'place_items';
    }

}