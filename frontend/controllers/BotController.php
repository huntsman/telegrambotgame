<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 12.07.2018
 * Time: 15:18
 */

namespace frontend\controllers;


use Yii;
use yii\web\Controller;
use aki\telegram;

class BotController extends Controller
{
    public function actionBotUpdate()
    {
        $updates = Yii::$app->telegram->getUpdates();
        var_export($updates);
    }

}